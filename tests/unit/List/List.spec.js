import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import List from './../../../src/components/list/List';

import { createLocalVue, mount } from '@vue/test-utils';
Vue.use(Vuetify);
Vue.use(Vuex);

describe('List Test', () => {
    const localVue = createLocalVue();
    let vuetify;
    let store;
    let actions = {
        addItemToBasket: jest.fn(),
        updateItemInBasket: jest.fn(),
        removeItemInBasket: jest.fn(),
        changeSnackbarStatu: jest.fn(),
    };
    let getters = {
        getBasket: function() {
            return [
                { id: 1, name: 'Şampuan', price: '13', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fsampuan.jpeg?v=1561027551321', amount: 2 },
                { id: 2, name: 'Deodorant', price: '26', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdeodorant.jpeg?v=1561027551696', amount: 2 },
                { id: 3, name: 'Diş Fırçası', price: '19.95', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798', amount: 2 },
            ];
        },
    };
    beforeEach(() => {
        vuetify = new Vuetify();
        store = new Vuex.Store({
            actions: actions,
            getters: getters,
        });
    });
    it('snapshot', () => {
        const wrapper = mount(List, {
            localVue,
            vuetify,
            propsData: {
                items: [
                    { id: 1, name: 'Şampuan', price: '13', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fsampuan.jpeg?v=1561027551321', amount: 2 },
                    { id: 2, name: 'Deodorant', price: '26', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdeodorant.jpeg?v=1561027551696', amount: 2 },
                    { id: 3, name: 'Diş Fırçası', price: '19.95', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798', amount: 2 },
                ],
            },
            store,
        });

        expect(wrapper.html()).toMatchSnapshot();
    });
    it('should have 3 listItem child components', () => {
        const wrapper = mount(List, {
            localVue,
            vuetify,
            propsData: {
                items: [
                    { id: 1, name: 'Şampuan', price: '13', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fsampuan.jpeg?v=1561027551321', amount: 2 },
                    { id: 2, name: 'Deodorant', price: '26', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdeodorant.jpeg?v=1561027551696', amount: 2 },
                    { id: 3, name: 'Diş Fırçası', price: '19.95', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798', amount: 2 },
                ],
            },
            store,
        });
        const components = wrapper.findAllComponents({ name: 'ListItem' });

        expect(components.length).toEqual(3);
    });
});
