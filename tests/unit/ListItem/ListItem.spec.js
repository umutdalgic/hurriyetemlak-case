import Vue from 'vue';
import Vuetify from 'vuetify';
import ListItem from './../../../src/components/listItem/ListItem';
import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';

Vue.use(Vuex);
Vue.use(Vuetify);

describe('ListItem Test', () => {
    const localVue = createLocalVue();
    let vuetify;
    let store;
    let actions = {
        addItemToBasket: jest.fn(),
        updateItemInBasket: jest.fn(),
        removeItemInBasket: jest.fn(),
        changeSnackbarStatu: jest.fn(),
    };
    beforeEach(() => {
        vuetify = new Vuetify();
        store = new Vuex.Store({
            actions: actions,
        });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('snapshout', () => {
        const wrapper = mount(ListItem, {
            localVue,
            vuetify,
            propsData: {
                item: {
                    id: 3,
                    name: 'Diş Fırçası',
                    price: '19.95',
                    currency: 'TRY',
                    image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                    amount: 2,
                },
            },
            computed: {
                shoppingCardItem() {
                    return {
                        id: 3,
                        name: 'Diş Fırçası',
                        price: '19.95',
                        currency: 'TRY',
                        image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                    };
                },
            },
        });
        expect(wrapper.html()).toMatchSnapshot();
    });

    it('item amount increment test', () => {
        const wrapper = mount(ListItem, {
            localVue,
            vuetify,
            propsData: {
                item: {
                    id: 3,
                    name: 'Diş Fırçası',
                    price: '19.95',
                    currency: 'TRY',
                    image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                },
            },
            store,
            computed: {
                shoppingCardItem() {
                    return {
                        id: 3,
                        name: 'Diş Fırçası',
                        price: '19.95',
                        currency: 'TRY',
                        amount: 1,
                        image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                    };
                },
            },
        });
        wrapper.vm.increment();
        expect(actions.updateItemInBasket).toHaveBeenCalled();
        expect(actions.changeSnackbarStatu).toHaveBeenCalled();
    });
    it('item amount decrement test', () => {
        const wrapper = mount(ListItem, {
            localVue,
            vuetify,
            propsData: {
                item: {
                    id: 3,
                    name: 'Diş Fırçası',
                    price: '19.95',
                    currency: 'TRY',
                    image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                },
            },
            computed: {
                shoppingCardItem() {
                    return {
                        id: 3,
                        name: 'Diş Fırçası',
                        price: '19.95',
                        currency: 'TRY',
                        amount: 2,
                        image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                    };
                },
            },
            store,
        });
        wrapper.vm.decrement();
        expect(actions.updateItemInBasket).toHaveBeenCalled();
        expect(actions.changeSnackbarStatu).toHaveBeenCalled();
    });
    it('item remove test', () => {
        const wrapper = mount(ListItem, {
            localVue,
            vuetify,
            propsData: {
                item: {
                    id: 3,
                    name: 'Diş Fırçası',
                    price: '19.95',
                    currency: 'TRY',
                    image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                },
            },
            computed: {
                shoppingCardItem() {
                    return {
                        id: 3,
                        name: 'Diş Fırçası',
                        price: '19.95',
                        currency: 'TRY',
                        amount: 1,
                        image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798',
                    };
                },
            },
            store,
        });
        wrapper.vm.decrement();
        expect(actions.updateItemInBasket).not.toHaveBeenCalled();
        expect(actions.changeSnackbarStatu).toHaveBeenCalled();
        expect(actions.removeItemInBasket).toHaveBeenCalled();
    });
});
