import Vue from 'vue';
import Vuetify from 'vuetify';
import SnackBar from './../../../src/components/snackBar/SnackBar';
import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import { store } from './../../../src/store/store';
Vue.use(Vuex);
Vue.use(Vuetify);

describe('SnackBar Test', () => {
    const localVue = createLocalVue();
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });
    it('snapshot', () => {
        const wrapper = mount(SnackBar, {
            localVue,
            vuetify,
            store,
        });

        expect(wrapper.html()).toMatchSnapshot();
    });
});
