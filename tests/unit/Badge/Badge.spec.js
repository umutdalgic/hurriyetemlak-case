import Vue from 'vue';
import Vuetify from 'vuetify';
import Bagde from './../../../src/components/badge/Badge';
import { createLocalVue, mount } from '@vue/test-utils';
Vue.use(Vuetify);

describe('Bagde Test', () => {
    const localVue = createLocalVue();
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });

    it('should have a custom count and match snapshot', () => {
        const wrapper = mount(Bagde, {
            localVue,
            vuetify,
            propsData: { count: 1 },
        });

        expect(wrapper.html()).toMatchSnapshot();

        const title = wrapper.find('.v-badge__wrapper');
        expect(title.text()).toBe('1');
    });
});
