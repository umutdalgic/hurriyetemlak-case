import Vue from 'vue';
import Vuetify from 'vuetify';
import Counter from './../../../src/components/counter/Counter';
import { createLocalVue, mount } from '@vue/test-utils';
Vue.use(Vuetify);

describe('Counter Test', () => {
    const localVue = createLocalVue();
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });

    it('should have a custom count and match snapshot', () => {
        const wrapper = mount(Counter, {
            localVue,
            vuetify,
            propsData: { data: 1 },
        });
        expect(wrapper.html()).toMatchSnapshot();
        wrapper.vm.$emit('decrement');
        wrapper.vm.$emit('increment');
        expect(wrapper.vm.data).toBe(1);
        expect(wrapper.emitted().decrement).toBeTruthy();
        expect(wrapper.emitted().increment).toBeTruthy();
    });
});
