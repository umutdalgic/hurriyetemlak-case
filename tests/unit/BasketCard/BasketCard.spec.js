import Vue from 'vue';
import Vuetify from 'vuetify';
import BasketCard from './../../../src/components/basketCard/BasketCard';
import { createLocalVue, mount } from '@vue/test-utils';
Vue.use(Vuetify);

describe('BasketCard Test', () => {
    const localVue = createLocalVue();
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });

    it('should view loading part', () => {
        const wrapper = mount(BasketCard, {
            localVue,
            vuetify,
            propsData: { loading: true },
        });

        expect(wrapper.html()).toMatchSnapshot();

        const loading = wrapper.find('.custom-loading');
        expect(loading.exists()).toBe(true);
    });
    it('should not view loading part', () => {
        const wrapper = mount(BasketCard, {
            localVue,
            vuetify,
            propsData: { loading: false },
        });

        expect(wrapper.html()).toMatchSnapshot();

        const loading = wrapper.find('.custom-loading');
        expect(loading.exists()).toBe(false);
    });

    it('should  view no result text', () => {
        const wrapper = mount(BasketCard, {
            localVue,
            vuetify,
            propsData: { loading: false, items: [] },
        });

        expect(wrapper.html()).toMatchSnapshot();

        const result = wrapper.find('.no-result');
        expect(result.text()).toBe('Alışveriş listenizde ürün bulunmamaktadır.');
    });

    it('should have 3 basketItem child components', () => {
        const wrapper = mount(BasketCard, {
            localVue,
            vuetify,
            propsData: {
                loading: false,
                items: [
                    { id: 1, name: 'Şampuan', price: '13', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fsampuan.jpeg?v=1561027551321', amount: 2 },
                    { id: 2, name: 'Deodorant', price: '26', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdeodorant.jpeg?v=1561027551696', amount: 2 },
                    { id: 3, name: 'Diş Fırçası', price: '19.95', currency: 'TRY', image: 'https://cdn.glitch.com/a28552e7-44e1-4bbd-b298-5745e70c2209%2Fdis-fircasi.jpeg?v=1561027551798', amount: 2 },
                ],
            },
        });
        const components = wrapper.findAllComponents({ name: 'BasketItem' });
        const result = wrapper.find('.no-result');

        expect(components.length).toEqual(3);

        expect(result.exists()).toBe(false);
    });
});
