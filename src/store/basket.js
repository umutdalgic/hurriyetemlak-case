import api from '../api/api';
const state = {
    basket: [],
};

const getters = {
    getBasket(state) {
        return state.basket;
    },
};

const actions = {
    addItemToBasket(context, item) {
        context.commit('ADD_ITEM_TO_BASKET_HANDLER', item);
    },
    updateItemInBasket(context, item) {
        context.commit('UPDATE_ITEM_TO_BASKET_HANDLER', item);
    },
    removeItemInBasket(context, item) {
        context.commit('REMOVE_ITEM_TO_BASKET_HANDLER', item);
    },
    submitBasket(context, item) {
        return new Promise((resolve, reject) => {
            api.post('order', item)
                .then((response) => {
                    resolve(response.data);
                })
                .catch(function(error) {
                    reject(error);
                });
        });
    },
    clearBasket(context) {
        context.commit('CLEAR_BASKET_HANDLER');
    },
};

const mutations = {
    ADD_ITEM_TO_BASKET_HANDLER(state, item) {
        state.basket = [...state.basket, ...[item]];
    },
    UPDATE_ITEM_TO_BASKET_HANDLER(state, item) {
        state.basket = state.basket.map((x) => {
            if (x.id == item.id) {
                x.amount = item.amount;
            }
            return x;
        });
    },
    REMOVE_ITEM_TO_BASKET_HANDLER(state, item) {
        state.basket = state.basket.filter((x) => x.id != item.id);
    },
    CLEAR_BASKET_HANDLER(state) {
        state.basket = [];
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
