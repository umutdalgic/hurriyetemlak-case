import Vue from 'vue';
import Vuex from 'vuex';

import shared from './shared';
import list from './list';
import basket from './basket';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        shared,
        list,
        basket,
    },
});
