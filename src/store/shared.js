const state = {
    snackbarStatu: null,
};

const getters = {
    snackbarStatu(state) {
        return state.snackbarStatu;
    },
};

const actions = {
    changeSnackbarStatu(context, item) {
        context.commit('SNACKBAR_STATU_HANDLER', item);
    },
};

const mutations = {
    SNACKBAR_STATU_HANDLER(state, item) {
        state.snackbarStatu = item;
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
