import api from '../api/api';
const state = {
    shoppingList: [],
};

const getters = {
    shoppingList(state) {
        return state.shoppingList;
    },
};

const actions = {
    getList() {
        return new Promise((resolve, reject) => {
            api.get('listing')
                .then((response) => {
                    resolve(response.data);
                })
                .catch(function(error) {
                    reject(error);
                });
        });
    },
};

const mutations = {
    changeLanguageHandler(state, lang) {
        state.selectedLocale = lang;
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
