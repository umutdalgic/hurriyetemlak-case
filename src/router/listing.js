export default [
    {
        path: '/',
        name: 'listing',
        component: () => import('./../views/Listing.vue'),
        meta: {
            layout: 'default',
        },
    },
];
