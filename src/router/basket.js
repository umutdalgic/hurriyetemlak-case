export default [
    {
        path: '/basket',
        name: 'basket',
        component: () => import('./../views/Basket.vue'),
        meta: {
            layout: 'default',
        },
    },
];
