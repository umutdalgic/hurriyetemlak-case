import Vue from 'vue';
import Router from 'vue-router';

import listing from './listing';
import basket from './basket';

let routerArr = [];

routerArr = routerArr.concat(listing).concat(basket);

Vue.use(Router);
export default new Router({
    mode: 'history',
    routes: routerArr,
});
