import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import { store } from './store/store';
import router from './router/router';
import Default from './layouts/Deafult.vue';
import 'vuetify/dist/vuetify.min.css';

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.component('default-layout', Default);

new Vue({
    store,
    router,
    vuetify: new Vuetify(),
    render: (h) => h(App),
    components: { App },
}).$mount('#app');
