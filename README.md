# hurriyetemlak-case

## Project setup
```
npm install
```

### Compiles and hot-reloads for development env
```
npm run serve
```

### Compiles and minifies for development env
```
npm run build
```

### Project Test Run
```
npm run test
```
